cda20156 / livColis_tm_jy
_________________________________________________________________________________________
Mini-projet cda-livColis
08/12/2020 - Roubaix.


Finalité de l'application :
_________________________________________________________________________________________
Création d'une application Web permettant de facilier la livraison et le suivi de colis.


L'application doit pouvoir proposer les fonctionnalités suivantes :


------------------------------------Pour les transporteurs------------------------------
- création d'un compte transporteur
- connexion à l'application
- mettre à jour leurs informations personnelles
- enregistrer un nouveau colis
- créer et/ou renseigner un émetteur et un destiataire
- créer et/ou renseigner le ou les points-relais concernés par la livraison
- génération d'un code barre utile au suivi livraison des colis


-----------------------------------Pour les utilisateurs---------------------------------
- renseigner leur numéro de colis afin d'avoir le suivi de celui-ci
- émetteur et destinataire reçoivent un mail de confirmation d'envoi du colis avec le
numéro de suivi


-----------------------------------La partie Gestion-------------------------------------
- menu "Mes colis" : le transporteur peut accéder aux colis qu'il a enregistrés, les colis
livrés, les colis en attente de prise en charge
- les transporteurs peuvent, lorsqu'ils sont connectés, accéder à la "Gestion Utilisateur"
=> permet de créer, modifier et de supprimer des utilisateurs enregistrés
- les transporteurs peuvent, lorsqu'ils sont connectés, accéder à la "Gestion Point Relais"
=> permet de créer, de modifier et de supprimer des points relais enregistrés


----------------------------------Première utilisation------------------------------------
- lors du premier accès à l'application, le transporteur doit créer son compte en cliquant
en haut à gauche sur "Espace transporteur" puis remplir le formulaire d'inscription


Suivi du projet :
_________________________________________________________________________________________
Le projet est disponible sur le GitKraken sur lequel vous êtes en admin, avec le nom :
livcolis_tm_jy


Conception du projet :
_________________________________________________________________________________________
Les diagrammes UML (de cas d'utilisation, de séquence et de classes) sont également
disponibles sur le tableau de bord Trello.


Démarche suivie :
_________________________________________________________________________________________
Lecture du projet et assimilation des concepts attendus.
Conception des diagrammes UML de cas d'utilisation, de séquence et de classes en solo.
Rencontre entre les deux contributeurs pour une mise en commun de la compréhension du projet.
Création de la maquette grâce au logiciel Adobe XD.
Création du repository et partage via BitBucket.
Partage des tâches et mise en commun via GitKraken.


Pré-requis :
________________________________________________________________________________________

Afin de lancer l'application, il faut récupérer l'ensemble du dossier livrable disponible
sur le GitKraken et obtenir le fichier .war

Il sera nécessaire de créer une base de données PostgreSql appelée "LivColis", avec
l'utilisateur "testCda" avec les droits de superuser et le mot de passe "123".

Sur cette base de données, entrez la requête suivante :


Aorès avoir lancé l'application et créé la base de données, exécutez la requête suivante :

INSERT INTO public.statut_etape_dao(
    id_statut, statut_name)
    VALUES (nextval('statut_seq'), 'LIVRE'), (nextval('statut_seq'), 'EN TRANSIT'),
	(nextval('statut_seq'), 'EN ATTENTE') ;


Contributeurs :
_________________________________________________________________________________________
Juliette Yguel
Thomas Mazzini

©JT - 2020