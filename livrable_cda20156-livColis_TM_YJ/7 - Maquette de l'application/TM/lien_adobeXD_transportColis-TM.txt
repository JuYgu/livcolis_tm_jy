Lien pour accéder à la maquette de transportColis-TM :

https://xd.adobe.com/view/8cfd5149-0d89-4819-aa33-2f2c23e4aed5-d9ae/


Pour le suivi de colis côté destinataire : cliquer sur "Suivi colis" disponible en bas à doite sur la page d'accueil. Sur la page de suivi, cliquer sur "Quitter" pour revenir sur le page d'accueil.

Par accéder à l'espace transporteur depuis la page d'accueil, cliquez directement sur "Espace transporteur" en haut à gauche.
Cliquez ensuite sur "Connexion" pour parvenir au tableau de bord d'un transporteur connecté.
Cliquez sur "Créer un compte pour accéder au formulaire d'inscrition d'un nouveau transporteur.


Servez-vous du menu disposé sur la gauche pour accéder aux rubriques souhaitées.


A tout moment, vous pouvez revenir sur la page d'accueil en cliquant sur "Déconnexion".
Vous pouvez également revenir sur la première page d'un transporteur connecté en cliquant sur "Mes colis".