package fr.afpa;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

import fr.afpa.livcolis.controller.beans.ColisBean;
import fr.afpa.livcolis.controller.beans.PointRelaisBean;
import fr.afpa.livcolis.controller.beans.TransBean;
import fr.afpa.livcolis.controller.beans.TransConnBean;
import fr.afpa.livcolis.controller.beans.UserBean;
import fr.afpa.livcolis.services.ColisService;
import fr.afpa.livcolis.services.EtapeLivService;
import fr.afpa.livcolis.services.PointRelaisService;
import fr.afpa.livcolis.services.StatutEtapeService;
import fr.afpa.livcolis.services.TransporteurService;
import fr.afpa.livcolis.services.UserService;
import fr.afpa.livcolis.services.UserServiceImp;
import fr.afpa.livcolis.services.beans.ColisDto;
import fr.afpa.livcolis.services.beans.EtapeLivraisonDto;
import fr.afpa.livcolis.services.beans.StatutEtapeDto;
import fr.afpa.livcolis.services.beans.TransDto;

@SpringBootTest
class LivColiszColisTests {

	@Autowired
	@Qualifier("colisService")
	ColisService cServ;
	
	 @Autowired
	 @Qualifier("userService")
	 UserService uServ;
	 @Autowired
	 UserServiceImp uServImpl;
	 
	@Autowired
	@Qualifier("pointRelaisService")
	PointRelaisService prServ;
	
	@Autowired
	@Qualifier("etapeLivService")
	EtapeLivService eServ;
	
	@Autowired
	@Qualifier("transporteurService")
	TransporteurService tServ;

	@Autowired
	@Qualifier("statutEtapeService")
	StatutEtapeService seServ;
	
	
	
 @Test
 public void saveColis() {
	 
	 UserBean emetteur  = UserBean.builder()
								 .nom("test1")
								 .prenom("test1")
								 .mail("test1@gmail.com")
								 .telephone("0000000001")
								 .ville("test1")
								 .voie("test1")
								 .codePostal("00000")
								 .build();
	 uServ.save(emetteur);
	 Long idEmetteur = uServ.findUser(emetteur);
	 UserBean destinataire  = UserBean.builder()
									 .nom("test2")
									 .prenom("test2")
									 .mail("test2@gmail.com")
									 .telephone("0000000002")
									 .ville("test2")
									 .voie("test2")
									 .codePostal("00001")
									 .build();
	 
	 uServ.save(destinataire);
	 Long idDestinataire = uServ.findUser(destinataire);
	 PointRelaisBean prBean = PointRelaisBean.builder()
											.telephone("0000000000")
											.voie("test")
											.ville("test")
											.codePostal("00000")
											.ouverture("08:00")
											.fermeture("18:00")
											.build();

	 prServ.save(prBean);
	 Long idPr = prServ.findPointRelais(prBean);
	 

	 ColisBean cBean = ColisBean.builder()
			 				 .poids(12F)
			 				 .prix(46F)
			 				 .build();
	 		 				 
	ColisDto cDto = cServ.save(cBean);
	cDto.setDestinataire( uServ.findById(idDestinataire));
	cDto.setEmetteur( uServ.findById(idEmetteur));
	
	assertEquals(cDto.getPoids(), cBean.getPoids());
	assertEquals(cDto.getEmetteur().getMail(), emetteur.getMail());
	assertEquals(cDto.getDestinataire().getMail(), destinataire.getMail());
	
	
	TransBean tBean = TransBean.builder()
				.login("test")
				.nom("test")
				.password("test")
				.mail("test@gmail.com")
				.telephone("0000000000")
				.build();

	tServ.save(tBean);
	 TransConnBean ConnectionBean = TransConnBean.builder()
												 .login("test")
												 .password("test")
												 .build();

	 Optional<TransDto> trans =tServ.findByLogin(ConnectionBean);
	

	 
	 List<StatutEtapeDto> etapesStatut = seServ.gatAllStatut();
	 assertNotNull(etapesStatut);
	 assertFalse(etapesStatut.isEmpty());
	 assertEquals(3 , etapesStatut.size());
	 assertEquals("LIVRE", etapesStatut.get(0).getStatutName());
	 
	 EtapeLivraisonDto etape = EtapeLivraisonDto.builder()
				.num(1)
				.transporteur(trans.get())
				.statutEtape(etapesStatut.get(0))
				.pointRelaisDao(prServ.findById(idPr))
				.colis(cServ.findById(cDto.getIdColis()))
				.build();

	 eServ.save(etape);
	 
	 EtapeLivraisonDto etapeTrouvee = eServ.findByIdColisAndIdPointRelais(cDto.getIdColis(), prServ.findPointRelais(prBean));
	 assertEquals(etape.getNum(), etapeTrouvee.getNum());
	 assertEquals(etape.getPointRelaisDao().getCodePostal(), etapeTrouvee.getPointRelaisDao().getCodePostal());
	 assertEquals(etape.getTransporteur().getIdTransporteur(), etapeTrouvee.getTransporteur().getIdTransporteur());
	 assertEquals(etape.getStatutEtape().getStatutName(), etapeTrouvee.getStatutEtape().getStatutName());
	 
	 etapeTrouvee = eServ.findById(etapeTrouvee.getIdEtape());
	 assertEquals(etape.getNum(), etapeTrouvee.getNum());
	 assertEquals(etape.getPointRelaisDao().getCodePostal(), etapeTrouvee.getPointRelaisDao().getCodePostal());
	 assertEquals(etape.getTransporteur().getIdTransporteur(), etapeTrouvee.getTransporteur().getIdTransporteur());
	 assertEquals(etape.getStatutEtape().getStatutName(), etapeTrouvee.getStatutEtape().getStatutName());

	 cDto.setEtapesLivraison(Arrays.asList(etapeTrouvee));
	 cServ.save(cDto);
	 
	 cServ.delete(cDto.getIdColis());
	 uServ.delete(idDestinataire);
	 uServ.delete(idEmetteur);
	 prServ.delete(idPr);
	 tServ.delete(trans.get().getIdTransporteur());
			 				
	
	 
 }
 
}