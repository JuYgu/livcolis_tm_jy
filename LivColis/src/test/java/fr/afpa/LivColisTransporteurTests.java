package fr.afpa;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;

import fr.afpa.livcolis.controller.beans.PointRelaisBean;
import fr.afpa.livcolis.controller.beans.TransBean;
import fr.afpa.livcolis.controller.beans.TransConnBean;
import fr.afpa.livcolis.services.PointRelaisService;
import fr.afpa.livcolis.services.PointRelaisServiceImpl;
import fr.afpa.livcolis.services.TransporteurService;
import fr.afpa.livcolis.services.TransporteurServiceImpl;
import fr.afpa.livcolis.services.beans.TransDto;

@SpringBootTest
class LivColisTransporteurTests {

	@Autowired
	@Qualifier("transporteurService")
	TransporteurService tServ;
	
 @Test
 public void saveAndGetAndUpdateTransporteur() {
	 TransBean tBean = TransBean.builder()
			 					.login("test")
			 					.nom("test")
			 					.password("test")
			 					.mail("test@gmail.com")
			 					.telephone("0000000000")
			 					.build();
	
	 tServ.save(tBean);
	 
	 TransConnBean ConnectionBean = TransConnBean.builder()
			 									 .login("test")
			 									 .password("test")
			 									 .build();
	 
	 Optional<TransDto> ok =tServ.findByLogin(ConnectionBean);
	 
	 assertTrue(ok.isPresent());
	 TransDto trans = tServ.findById(ok.get().getIdTransporteur());
	 assertEquals(tBean.getLogin(), trans.getLogin());
	 assertEquals(tBean.getPassword(), trans.getPassword());
	 
	 TransConnBean ConnectionBean2 = TransConnBean.builder()
												 .login("test test")
												 .password("test")
												 .build();
	 
	 Optional<TransDto> ok2 =tServ.findByLogin(ConnectionBean2);
	 assertFalse(ok2.isPresent());
	 TransBean tBean2 = TransBean.builder()
			 					 .idTransporteur(trans.getIdTransporteur())
								 .login("test test")
								 .nom("test test")
								 .password("test test")
								 .mail("test@gmail.com")
								 .telephone("0000000000")
								 .build();
	 
	 tServ.update(tBean2);
	 TransDto trans2 = tServ.findById(ok.get().getIdTransporteur());
	 assertEquals(trans2.getNom(),tBean2.getNom());
	 assertEquals(trans2.getPassword(), tBean2.getPassword());
	 assertEquals(trans2.getLogin(), tBean2.getLogin());
	 assertEquals(trans2.getMail(), tBean2.getMail());
	 assertEquals(trans2.getTelephone(), tBean2.getTelephone());
	 
	 tServ.delete(trans2.getIdTransporteur());
	 TransDto trans3 = tServ.findById(trans2.getIdTransporteur());
	 assertNull(trans3);
	 
	 
	 
	 
 }
 
}