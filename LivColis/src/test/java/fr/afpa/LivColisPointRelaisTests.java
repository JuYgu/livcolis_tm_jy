package fr.afpa;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import fr.afpa.livcolis.controller.beans.PointRelaisBean;
import fr.afpa.livcolis.services.PointRelaisService;
import fr.afpa.livcolis.services.PointRelaisServiceImpl;
import fr.afpa.livcolis.services.beans.PointRelaisDto;

@SpringBootTest
public class LivColisPointRelaisTests {

	@Autowired
	@Qualifier("pointRelaisService")
	PointRelaisService prServ;
	
	@Test
 	public void saveAndGetPointRelais() {
 		
 		PointRelaisBean prBean = PointRelaisBean.builder()
												.telephone("0000000000")
												.voie("test test")
												.ville("test")
												.codePostal("59000")
												.ouverture("08:00")
												.fermeture("18:00")
												.build();

 		prServ.save(prBean);
 		
 		
 		Page<PointRelaisDto> find = prServ.findByVille("test", PageRequest.of(0,  5));
 		assertFalse(find.isEmpty());
 		assertEquals(5, find.getSize());
 		assertEquals(1, find.getContent().size());
 		List<PointRelaisDto> listPr = find.getContent();
 		assertEquals(prBean.getVille(), find.getContent().get(0).getVille());
 		assertEquals(prBean.getVoie(), find.getContent().get(0).getVoie());
 		assertEquals(prBean.getTelephone(), find.getContent().get(0).getTelephone());
 		assertEquals(prBean.getCodePostal(), find.getContent().get(0).getCodePostal());
 		assertEquals(prBean.getOuverture(), find.getContent().get(0).getOuverture());
 		assertEquals(prBean.getFermeture(), find.getContent().get(0).getFermeture());

 		Long id = prServ.findPointRelais(prBean);
 		PointRelaisDto pr2 = prServ.findById(id);
 		pr2.setVille("test test");
 		prServ.update(pr2);
 		PointRelaisDto prDto2 = prServ.findById(pr2.getIdPointRelais());
 		assertEquals(pr2.getVille(), prDto2.getVille());
 		assertNotEquals(prBean.getVille(), pr2.getVille());
 		
 		prServ.delete(pr2.getIdPointRelais());
 		find = prServ.findByVille("test test", PageRequest.of(0, 5));
 		
 		assertTrue(find.isEmpty());
 	}
	
}