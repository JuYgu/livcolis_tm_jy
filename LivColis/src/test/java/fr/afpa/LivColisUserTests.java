package fr.afpa;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import fr.afpa.livcolis.controller.beans.UserBean;
import fr.afpa.livcolis.services.UserService;
import fr.afpa.livcolis.services.UserServiceImp;
import fr.afpa.livcolis.services.beans.UserDto;

@SpringBootTest
class LivColisUserTests {


	 @Autowired
	 @Qualifier("userService")
	 UserService uServ;
	 @Autowired
	 UserServiceImp uServImpl;
	
	 @Test
	 public void saveAndGetAndUpdateAndDeleteUser() {
		UserBean uBean = UserBean.builder()
				 				 .nom("test")
				 				 .prenom("test")
				 				 .mail("test@gmail.com")
				 				 .telephone("0000000000")
				 				 .ville("test")
				 				 .voie("test")
				 				 .codePostal("00000")
				 				 .build();
		
		 uServ.save(uBean);
		 
		
		 
		 Page<UserDto> ok =uServ.findByNom("test", PageRequest.of(0, 5) );
		 
		 assertEquals(5, ok.getSize());
		 assertEquals(1, ok.getContent().size());
		 UserDto uDtoTest = uServ.findById(ok.getContent().get(0).getIdUser());
		 assertEquals(uBean.getPrenom(), uDtoTest.getPrenom());
		 assertEquals(uBean.getNom(), uDtoTest.getNom());
		 assertEquals(uBean.getMail(), uDtoTest.getMail());
		 assertEquals(uBean.getTelephone(), uDtoTest.getTelephone());
		 assertEquals(uBean.getVille(), uDtoTest.getVille());
		 assertEquals(uBean.getVoie(), uDtoTest.getVoie());
		 assertEquals(uBean.getCodePostal(), uDtoTest.getCodePostal());
		 
		 uDtoTest.setNom("test test");
		 uServ.update(uDtoTest);
		 uBean = UserBean.builder()
 				 .nom("test test")
 				 .prenom("test")
 				 .mail("test@gmail.com")
 				 .build();
		 Long id = uServ.findUser(uBean);
		 UserDto uDto = uServ.findById(id);
		 assertEquals(uDto.getNom(),  uDtoTest.getNom());
		 
		 uServ.delete(id);
		 ok = uServ.findByNom("test test", PageRequest.of(0, 5));
		 
		 assertTrue(ok.getContent().isEmpty());
	 }
	

}
