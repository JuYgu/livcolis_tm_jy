package fr.afpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LivColisApplication {

	public static void main(String[] args) {
		SpringApplication.run(LivColisApplication.class, args);
	}

}
