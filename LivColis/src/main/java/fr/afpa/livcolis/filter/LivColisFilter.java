package fr.afpa.livcolis.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class LivColisFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
    	HttpServletRequest req = (HttpServletRequest) request;
    	HttpServletResponse res = (HttpServletResponse) response;
    	HttpSession session = req.getSession();
    	 String chemin = req.getRequestURI().substring( req.getContextPath().length() );
    	
    	String path = req.getServletPath();
    	
    	if (session.getAttribute("currentTransporteur") != null || path.equals("/transporteur/inscription") || path.equals("/")  ||  path.equals("/state")  || path.equals("/transporteur/connexion") || chemin.startsWith("/css") || chemin.startsWith("/images") || chemin.startsWith("/js ") || chemin.startsWith("/webjars")) {
    		chain.doFilter(request, response);
    	} else {
    		 request.getRequestDispatcher( "/transporteur/connexion" ).forward( request, response );
    	}
		
	}

}
