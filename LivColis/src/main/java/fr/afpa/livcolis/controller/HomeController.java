package fr.afpa.livcolis.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;

import fr.afpa.livcolis.services.ColisService;
import fr.afpa.livcolis.services.beans.ColisDto;

@Controller
@RequestMapping("/")
public class HomeController {

	
	@Autowired
	@Qualifier("colisService")
	ColisService cServ;
	
	@GetMapping({"/", "/accueil"})
	public String pageAccueil() {
		return "accueil";
	}
	
	@GetMapping("/state")
	public String vueColis(@RequestParam("noColis") String codeBarre, Model model) {
		System.out.println("Je suis laaa");
		Optional<ColisDto> colis = cServ.findByCodeBarre(codeBarre);
		model.addAttribute("colis", codeBarre);
		System.out.println("Il y a dans mon colis : " + colis);
		if(colis.isPresent()) {
			model.addAttribute("etape", colis.get().getEtapesLivraison());
			model.addAttribute("colisLivraison", colis.get().isLivraisonDestinataire());
			
			model.addAttribute("colisLivraison", colis.get().isLivraisonDestinataire());
			
		}else {
			model.addAttribute("noexist", "noexist");
		}

		
		return "client-etatColis";
		
	}
	
	
	@GetMapping("/deconnexion")
	public String deconnexion(SessionStatus status, HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.invalidate();
		status.setComplete();
		return "redirect:/";
	}
}