package fr.afpa.livcolis.controller.beans;





import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@Getter @Setter

public class UserBean {
	private Long idUser;
	@NotBlank(message = "Champ obligatoire") @Size(min = 2, max = 30, message= "La taille du nom doit être comprise entre 2 et 30 caractères")
	private String nom;
	@NotBlank(message = "Champ obligatoire") @Size(min = 2, max = 30, message ="La taille du prénom doit être comprise entre 2 et 30 caractères")
	private String prenom;
	@Email @NotBlank(message = "Champ obligatoire")
	private String mail;
	@NotBlank(message = "Champ obligatoire") @Size(min = 10, max = 10, message = "Le numéro de téléphone doit comporter 10 chiffres")
	private String telephone;
	@NotBlank(message = "Champ obligatoire") @Size(min = 5, max = 50, message = "La taille doit être comprise entre 5 et 50 caractères")
	private String voie;
	@NotBlank(message = "Champ obligatoire") @Size(min = 5, max = 5, message = "Le code postal doit comporter 5 chiffres")
	private String codePostal;
	@NotBlank(message = "Champ obligatoire") @Size(min = 2, max = 50, message = "La taille de la ville doit être comprise entre 2 et 50 caractères")
	private String ville;
	
}
