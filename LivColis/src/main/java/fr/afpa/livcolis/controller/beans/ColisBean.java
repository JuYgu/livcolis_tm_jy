package fr.afpa.livcolis.controller.beans;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ColisBean {
	
	private Long idColis;
	private Date date;
	@NotNull @PositiveOrZero
	private float poids;
	@NotNull @PositiveOrZero
	private float prix;
	private String codeBarre;
	private String path;
	private boolean livraisonDestinataire;
	
}
