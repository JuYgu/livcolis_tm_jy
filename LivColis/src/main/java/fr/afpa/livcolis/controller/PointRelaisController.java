package fr.afpa.livcolis.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.afpa.livcolis.controller.beans.PointRelaisBean;
import fr.afpa.livcolis.services.PointRelaisService;
import fr.afpa.livcolis.services.PointRelaisServiceImpl;
import fr.afpa.livcolis.services.beans.PointRelaisDto;

@Controller
@RequestMapping("/pointrelais")
public class PointRelaisController {

	@Autowired
	@Qualifier("pointRelaisService")
	PointRelaisService prServ;
	

	
	/**
	 * Page permettant d'afficher le formulaire de création d'un nouveau Point Relais
	 * S'il s'agit d'une création à proprement parler, passage en new pour indiquer qu'il s'agit d'une création de Point Relais
	 * @param model
	 * @return pointRelaisNew
	 */
	@GetMapping("/new")
	public String create(Model model) {
		model.addAttribute("pointRelaisBean", new PointRelaisBean());
		
		System.out.println("Mode : "+model.getAttribute("mode"));
		if(model.getAttribute("mode") == null) {
			model.addAttribute("mode", "new");
		} else {
			model.addAttribute("mode", "edit");
		}
		System.out.println("Mode : "+model.getAttribute("mode"));
		return "gestionPointRelaisNew";
	}
	
	
	/**
	 * Méthode Post pour la récupération des données du formulaire de création d'un nouveau Point Relais
	 * Si des erreurs sont présentes dans le formulaire, retourne le formulaire
	 * Sauvegarde le Point Relais nouvellement créé en bdd et renvoie la page success page créée grâce au mode "new"
	 * @param pointRelaisBean
	 * @param br, atts
	 * @return "redirect:/pointrelais/success"
	 */
	@PostMapping("/new")
	public String create(@Valid PointRelaisBean pointRelaisBean, BindingResult br,Model model, RedirectAttributes atts) {
		if(br.hasErrors()) {
			System.out.println(pointRelaisBean);
			return "gestionPointRelaisNew";
		}
		
		prServ.save(pointRelaisBean);
		if("new".equals(model.getAttribute("mode"))) {
			atts.addFlashAttribute(pointRelaisBean);
			atts.addFlashAttribute("mode", "new");
			return "redirect:/pointrelais/success";
		}
		return "redirect:/pointrelais/recherche?search="+pointRelaisBean.getVille();

	}
	
	
	/**
	 * * Page permettant d'afficher le formulaire de Point Relais avec infos pré-remplies, afin de pouvoir le modifier
	 * On passe le mode en edit pour indiquer qu'il s'agit d'une modification de Point Relais existant
	 * @param model, id
	 * @return "pointRelaisNew"
	 */
	@GetMapping("/update")
	public String update(Model model, Long id) {
		
		model.addAttribute("pointRelaisBean", prServ.findById(id));
		model.addAttribute("mode", "edit");
		return "gestionPointRelaisNew";
	}
	
	
	/**
	 * Page "racine" de monurl/pointrelais, qui doit afficher la page de recherche de Points Relais
	 * @return
	 */
	@GetMapping("/")
	public String search() {
		return "gestionPointRelaisSearch";
	}
	
	
	/**
	 * Page permettant de rechercher un Point Relais en indiquant la ville souhaitée
	 * => va afficher tous les Point Relais de la ville sous forme de card si existants, sinon retourne "pas de valeur trouvée"
	 * @param model, page, size, search
	 * @return "pointRelaisSearch"
	 */
	@GetMapping("/recherche")
	public String searchAction(Model model,
			@RequestParam(name="page", defaultValue = "0") int page,
			@RequestParam(name="size", defaultValue = "80") int size,
			@RequestParam(name="search", defaultValue = "") String search) {
		
		model.addAttribute("mode", "recherche");
		
		Page<PointRelaisDto> pointRelaisPage = prServ.findByVille(search, PageRequest.of(page, size));
		if(!pointRelaisPage.isEmpty()) {
			model.addAttribute("listePointRelais", pointRelaisPage);
			model.addAttribute("pages", new int[pointRelaisPage.getTotalPages()]);
			model.addAttribute("currentPage", page);
			model.addAttribute("keyword", search);
			model.addAttribute("size", size);
			model.addAttribute("recherche", "value");
		} else {
			model.addAttribute("recherche", "noValue");
		}
		return "gestionPointRelaisSearch";
	}
	
	
	/**
	 * Pour afficher la page indiquant que le point relais a été créé ou modifié avec succès
	 * @return "pointRelaisSuccess"
	 */
	@GetMapping("/success")
	public String success() {
		return "pointRelaisSuccess";
	}
	
}