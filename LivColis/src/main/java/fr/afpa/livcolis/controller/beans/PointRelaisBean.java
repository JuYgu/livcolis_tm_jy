package fr.afpa.livcolis.controller.beans;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString

public class PointRelaisBean {

	private Long idPointRelais;
	@NotBlank(message = "Champ obligatoire") @Size(min=10, max=10, message = "Le numéro de téléphone doit comporter 10 chiffres")
	private String telephone;
	@NotBlank(message = "Champ obligatoire") @Size(min = 5, max = 50, message = "La taille doit être comprise entre 5 et 50 caractères")
	private String voie;
	@NotBlank(message = "Champ obligatoire") @Size(min = 2, max = 50, message = "La taille de la ville doit être comprise entre 2 et 50 caractères")
	private String ville;
	@NotBlank(message = "Champ obligatoire") @Size(min = 5, max = 5, message = "Le code postal doit comporter 5 chiffres")
	private String codePostal;
	@NotBlank(message = "Champ obligatoire") @Size(min = 1, max = 10, message = "Merci d'indiquer l\'heure d'ouverture")
	private String ouverture;
	@NotBlank(message = "Champ obligatoire") @Size(min = 1, max = 10, message = "Merci d'indiquer l\'heure de fermeture")
	private String fermeture;
	
}