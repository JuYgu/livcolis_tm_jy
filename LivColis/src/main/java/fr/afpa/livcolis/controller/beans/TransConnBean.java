package fr.afpa.livcolis.controller.beans;


import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class TransConnBean {
	 @NotNull
	private String login;
	 @NotNull
	private String password;
}
