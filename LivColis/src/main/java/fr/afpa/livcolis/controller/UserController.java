package fr.afpa.livcolis.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.afpa.livcolis.controller.beans.UserBean;
import fr.afpa.livcolis.services.UserService;
import fr.afpa.livcolis.services.UserServiceImp;
import fr.afpa.livcolis.services.beans.UserDto;

@Controller
@RequestMapping("/utilisateurs")
public class UserController {

	@Autowired
	@Qualifier("userService")
	UserService uServ;
	
	@Autowired
	private UserServiceImp uServImpl;
	

	@GetMapping("/new")
	public String create(Model model) {
		model.addAttribute("userBean", new UserBean());
		if(model.getAttribute("mode") == null) {
			model.addAttribute("mode", "new");
		}else {
			model.addAttribute("mode", "edit");
		}

		return "userNew";
	}

	@PostMapping("/new")
	public String create(@Valid UserBean userBean, BindingResult br, RedirectAttributes atts) {
		System.out.println(userBean);
		if (br.hasErrors()) {
			return "userNew";
		}
		
		uServ.save(userBean);
		atts.addFlashAttribute( userBean);
		atts.addFlashAttribute("mode", "new");
		return "redirect:/utilisateurs/success";
	}
	
	@GetMapping("/update")
	public String update(Model model,Long id){
		model.addAttribute("userBean", uServ.findById(id));
		model.addAttribute("mode", "edit");
		
		return  "userNew";
	}

	@GetMapping("/")
	public String search(Model model) {
		model.addAttribute("mode", "recherche");
		return "userSearch";
	}
	
	@GetMapping("/recherche")
	public String searchAction(Model model, 
						@RequestParam(name="page", defaultValue = "0") int page,
						@RequestParam(name="size", defaultValue = "5") int size,
						@RequestParam(name="search", defaultValue = "") String search) {
		
		model.addAttribute("mode", "recherche");
		
		Page<UserDto> userPage = uServ.findByNom(search, PageRequest.of(page, size));
		if(!userPage.isEmpty()) {
			model.addAttribute("listeUsers", userPage);
			model.addAttribute("pages", new int[userPage.getTotalPages()]);
			System.out.println(userPage.getTotalPages());
			model.addAttribute("currentPage", page);
			model.addAttribute("keyword", search);
			model.addAttribute("size", size);
			model.addAttribute("recherche", "value");

		}else {
			model.addAttribute("recherche", "noValue");
			
		}
		return "userSearch";
	}
	
	@GetMapping("/success")
	public String success() {
		return "userSuccess";
	}
	
	
	
}
