package fr.afpa.livcolis.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.afpa.livcolis.controller.beans.TransBean;
import fr.afpa.livcolis.controller.beans.TransConnBean;
import fr.afpa.livcolis.services.TransporteurService;
import fr.afpa.livcolis.services.beans.TransDto;

@Controller
@RequestMapping("/transporteur")
@SessionAttributes({"currentTransporteur", "colisTransporteur"})
public class TransporteurController {

	@Autowired
	@Qualifier("transporteurService")
	TransporteurService transServ;
	
	@GetMapping("/connexion")
	public String connexion(Model model) {
		model.addAttribute("transporteur", new TransConnBean());
		
		return "transConnexion";
	}
	
	@PostMapping("/connexion")
	public String connexion( @Valid TransConnBean transporteur, BindingResult br, Model model ) {
		if(br.hasErrors()) {
			return "transConnexion";
		}
		Optional<TransDto> optionalTrans = transServ.findByLogin(transporteur);
		if (optionalTrans.isPresent()) {
			model.addAttribute("currentTransporteur", optionalTrans.get().getIdTransporteur());
			return "redirect:/colis/mes-colis";  
		}else {
			model.addAttribute("transporteur",transporteur);
			model.addAttribute("identifiant", "incorrect");
			// Ajouter findTransporteurColis
			return "transConnexion";
		}
	}
	
	
	
	@GetMapping("/inscription")	
	public String inscription(Model model) {
		System.out.println("jojo");
		model.addAttribute("transporteur", new TransBean());
		model.addAttribute("mode", "new");
		return "transInscription";
	}
	
	
	@PostMapping("/inscription")	
	public String inscription(@Valid TransBean transporteur, BindingResult br, Model model, RedirectAttributes atts) {
		model.addAttribute("mode", "new");
		
		if(br.hasErrors()) {
			return "transInscription";
		}
		if(transServ.save(transporteur)) {
			atts.addFlashAttribute("transporteur", transporteur);
			System.out.println("jojo pinpin");
			return "redirect:/transporteur/inscription/success";
		}else {
			model.addAttribute("transporteur",transporteur);
			model.addAttribute("login", "existant");
			return "transInscription";
		}
	}
	
	
	@GetMapping("/profil")
	public String profil(Model model) {
		Long idTransporteur = (Long) model.getAttribute("currentTransporteur");
		TransDto tDto = transServ.findById(idTransporteur);
		model.addAttribute("mode", "profil");
		model.addAttribute("login", "ok");
		model.addAttribute("transporteur", tDto);
		return "transProfil";
	}
	
	
	@GetMapping("/profil/modification")
	public String update(Model model) {
		Long idTransporteur = (Long) model.getAttribute("currentTransporteur");
		TransDto tDto = transServ.findById(idTransporteur);
		model.addAttribute("transporteur", tDto);
		model.addAttribute("mode", "edit");
		return "transUpdate";
	}

	@PostMapping("/profil/modification")
	public String update(@Valid TransBean transporteur,  BindingResult br, Model model ) {
		model.addAttribute("mode", "edit");
		model.addAttribute("transporteur", transporteur);
		if ( br.hasErrors()) {
			return "transUpdate";
		}
		System.out.println("jojo");
		Long id = (Long) model.getAttribute("currentTransporteur");
		transporteur.setIdTransporteur(id);
		transServ.update(transporteur);
		return "redirect:/transporteur/profil";
		
	}
	
	@GetMapping("/inscription/success")	
	public String success() {
		return "transInscSuccess";
	}
	
	
}
