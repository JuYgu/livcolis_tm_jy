package fr.afpa.livcolis.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.afpa.livcolis.controller.beans.ColisBean;
import fr.afpa.livcolis.controller.beans.PointRelaisBean;
import fr.afpa.livcolis.controller.beans.UserBean;
import fr.afpa.livcolis.services.ColisService;
import fr.afpa.livcolis.services.EtapeLivService;
import fr.afpa.livcolis.services.PointRelaisService;
import fr.afpa.livcolis.services.StatutEtapeService;
import fr.afpa.livcolis.services.TransporteurService;
import fr.afpa.livcolis.services.UserService;
import fr.afpa.livcolis.services.beans.ColisDto;
import fr.afpa.livcolis.services.beans.EtapeLivraisonDto;
import fr.afpa.livcolis.services.beans.PointRelaisDto;
import fr.afpa.livcolis.services.beans.StatutEtapeDto;
import fr.afpa.livcolis.services.beans.UserDto;

@Controller
@RequestMapping("/colis")
@SessionAttributes({"currentTransporteur", "colisTransporteur", "currentColis", "currentEmetteur", "currentDestinataire", "currentPointRelais", "currentEtapeLivraison", "statutEtapeList"})
public class ColisController {

	@Autowired
	@Qualifier("colisService")
	ColisService cServ;
	
	@Autowired
	@Qualifier("userService")
	UserService uServ;
	
	@Autowired
	@Qualifier("pointRelaisService")
	PointRelaisService prServ;

	@Autowired
	@Qualifier("etapeLivService")
	EtapeLivService elServ;
	
	@Autowired
	@Qualifier("transporteurService")
	TransporteurService tServ;
	
	@Autowired
	@Qualifier("statutEtapeService")
	StatutEtapeService sServ;
/*
 * METHODES DE CREATION DE COLIS------------------------------------------------------------------------------------------------------------------------------------
 */
	
	
	/*
	 * CREATION COLIS-----------------------------------------------------------
	 */
	@GetMapping("/new")
	public String createColis(Model model) {
		model.addAttribute("colis", new ColisBean());
		
		model.addAttribute("statutEtapeList", sServ.gatAllStatut());
		

		return "colisForm";
	}
	
	@PostMapping("/new")
	public String createColis(@Valid ColisBean colis, BindingResult br, Model model) {
		
		if (br.hasErrors()) {
			return "colisForm";
		}
		colis.setDate(new Date(System.currentTimeMillis()));
		
		ColisDto colisDto = cServ.save(colis);
		model.addAttribute("currentColis", colisDto.getIdColis());

		return "userEmetteurSearch";	
	}
	
	/*
	 * RECHERCHE CREATION EMETTEUR----------------------------------------------------------
	 */
	@GetMapping("/new/emetteur")
	public String searchEmetteur(Model model) {
		
		model.addAttribute("mode", "/colis/new/emetteur/recherche");
		model.addAttribute("retour", "validationEmetteur");
		return "userEmetteurSearch";
		
	}
	
	@GetMapping("/new/emetteur/recherche")
	public String searchEmetteurAction(Model model, 
			@RequestParam(name="page", defaultValue = "0") int page,
			@RequestParam(name="size", defaultValue = "80") int size,
			@RequestParam(name="search", defaultValue = "") String search) {
		
		model.addAttribute("mode", "/colis/new/emetteur/recherche");
		model.addAttribute("retour", "validationEmetteur");
		Page<UserDto> userPage = uServ.findByNom(search, PageRequest.of(page, size));
		
		if(!userPage.isEmpty()) {
			model.addAttribute("listeUsers", userPage);
			model.addAttribute("pages", new int[userPage.getTotalPages()]);
			System.out.println(userPage.getTotalPages());
			model.addAttribute("currentPage", page);
			model.addAttribute("keyword", search);
			model.addAttribute("size", size);
			model.addAttribute("recherche", "value");

		}else {
			model.addAttribute("recherche", "noValue");
		}
		
		return "userEmetteurSearch";
	}
	
	@GetMapping("/new/emetteur/validationEmetteur")
	public String validationEmetteur(Model model,  @RequestParam(name = "id") String search) {
		model.addAttribute("currentEmetteur", Long.parseLong(search));
		model.addAttribute("retour", "validationDestinataire");
		return "redirect:/colis/new/destinataire";
	}
	
	@GetMapping("/new/emetteur/new")
	public String createEmetteur(Model model) {
		model.addAttribute("userBean", new UserBean());
		model.addAttribute("mode", "new");
		return "userEmetteurNew";
	}

	@PostMapping("/new/emetteur/new")
	public String createEmetteur(@Valid UserBean userBean, BindingResult br, RedirectAttributes atts, Model model) {
		System.out.println(userBean);
		if (br.hasErrors()) {
			return "userEmetteurNew";
		}
		uServ.save(userBean);
		model.addAttribute("currentEmetteur", uServ.findUser(userBean)); 
		return "redirect:/colis/new/destinataire";

	}
	
	
	/*
	 * RECHERCHE CREATION DESTINATAIRE----------------------------------------------------------------
	 */
	@GetMapping("/new/destinataire")
	public String searchDestinataire(Model model) {
		model.addAttribute("retour", "validationDestinataire");
		model.addAttribute("mode", "colis/new/destinataire/recherche");
		return "userDestinataireSearch";
		
	}
	
	@GetMapping("/new/destinataire/recherche")
	public String searchDestinataireAction(Model model, 
			@RequestParam(name="page", defaultValue = "0") int page,
			@RequestParam(name="size", defaultValue = "80") int size,
			@RequestParam(name="search", defaultValue = "") String search) {
		model.addAttribute("retour", "validationDestinataire");
		model.addAttribute("mode", "colis/new/destinataire/recherche");
		Page<UserDto> userPage = uServ.findByNom(search, PageRequest.of(page, size));
		
		if(!userPage.isEmpty()) {
			model.addAttribute("listeUsers", userPage);
			model.addAttribute("pages", new int[userPage.getTotalPages()]);
			System.out.println(userPage.getTotalPages());
			model.addAttribute("currentPage", page);
			model.addAttribute("keyword", search);
			model.addAttribute("size", size);
			model.addAttribute("recherche", "value");

		}else {
			model.addAttribute("recherche", "noValue");
		}
		
		return "userDestinataireSearch";
	}
	
	@GetMapping("/new/destinataire/validationDestinataire")
	public String validationDestinataire( Model model,  @RequestParam(name = "id") String search) {
		model.addAttribute("currentDestinataire", Long.parseLong(search));
		return "redirect:/colis/new/pointRelais";
	}
	
	
	@GetMapping("/new/destinataire/new")
	public String createDestinataire(Model model) {
		model.addAttribute("userBean", new UserBean());
		model.addAttribute("mode", "new");
		return "userDestinataireNew";
	}

	@PostMapping("/new/destinataire/new")
	public String createDestinataire(@Valid UserBean userBean, BindingResult br, RedirectAttributes atts, Model model) {
		System.out.println(userBean);
		if (br.hasErrors()) {
			return "userDestinataireNew";
		}
		uServ.save(userBean);
		model.addAttribute("currentDestinataire", uServ.findUser(userBean)); 
		
		return "redirect:/colis/new/pointRelais";
	}
	
	
	/*
	 * RECHERCHE CREATION POINT RELAIS-------------------------------------------------------------------------------------
	 *
	// ---> Soit le client créé la point relais // a la fin du choix d'id
	//--->  model.addAttribute("currentPointRelais", prServ.findPointRelais(prBean)); 
	//---> return "etapeNew"
	 */
	
	@GetMapping("/new/pointRelais")
	public String searchPointRelais(Model model) {
		
		model.addAttribute("mode", "/colis/new/pointRelais/recherche");
		model.addAttribute("action", "/colis/new/pointRelais/validationPointRelais");
		model.addAttribute("nouveau", "/colis/new/pointRelais/new");
		return "pointRelaisSearch";
	}
	
	
	@GetMapping("/new/pointRelais/recherche")
	public String searchPointRelaisAction(Model model,
			@RequestParam(name="page", defaultValue = "0") int page,
			@RequestParam(name="size", defaultValue = "80") int size,
			@RequestParam(name="search", defaultValue = "") String search) {
		model.addAttribute("mode", "/colis/new/pointRelais/recherche");
		model.addAttribute("action", "/colis/new/pointRelais/validationPointRelais");
		model.addAttribute("nouveau", "/colis/new/pointRelais/new");
		Page<PointRelaisDto> pointRelaisPage = prServ.findByVille(search, PageRequest.of(page, size));
		
		if(!pointRelaisPage.isEmpty()) {
			model.addAttribute("listePointRelais", pointRelaisPage);
			model.addAttribute("pages", new int[pointRelaisPage.getTotalPages()]);
			model.addAttribute("currentPage", page);
			model.addAttribute("keyword", search);
			model.addAttribute("size", size);
			model.addAttribute("recherche", "value");
		} else {
			model.addAttribute("recherche", "noValue");
		}
		return "pointRelaisSearch";
	}
	
	
	@GetMapping("/new/pointRelais/validationPointRelais")
	public String validationDPointRelais(Model model, @RequestParam(name = "id") String search) {
		model.addAttribute("currentPointRelais", Long.parseLong(search));
		return "redirect:/colis/new/etape";
	}


	@GetMapping("/new/pointRelais/new")
	public String createPointRelais(Model model) {
		model.addAttribute("pointRelaisBean", new PointRelaisBean());
		model.addAttribute("mode", "new");
		model.addAttribute("action", "/colis/new/pointRelais/new");
		model.addAttribute("retour", "/colis/new/pointRelais");
		return "pointRelaisNew";
	}

	
	@PostMapping("/new/pointRelais/new")
	public String createPointRelais(@Valid PointRelaisBean pointRelaisBean, BindingResult br, RedirectAttributes atts, Model model) {
		System.out.println(pointRelaisBean);
		if (br.hasErrors()) {
			return "pointRelaisNew";
		}
		prServ.save(pointRelaisBean);
		model.addAttribute("currentPointRelais", prServ.findPointRelais(pointRelaisBean)); 
		return "redirect:/colis/new/etape";
	}




/*
 * CREATION ETAPE LIVRAISON----------------------------------------------------------
 */

	@GetMapping("/new/etape")
	public String createEtape(Model model) {
		model.addAttribute("numEtape", 1);
		model.addAttribute("etape", EtapeLivraisonDto.builder().build());
		model.addAttribute("action", "/colis/new/etape");
		
		return "etapeForm";
	}
	
	@PostMapping("/new/etape")
	public String createEtape(EtapeLivraisonDto etape, BindingResult br, Model model ) {
		System.out.println("etape A la sortie du four : "+etape);

		if (br.hasErrors()) {
			System.out.println("Errors sa maman");
			model.addAttribute("etape", etape);
			model.addAttribute("action", "/colis/new/etape");
			return "etapeForm";
		}
		
		
		etape.setTransporteur(tServ.findById((Long)model.getAttribute("currentTransporteur")));
		Long id = (Long) model.getAttribute("currentPointRelais");
		etape.setPointRelaisDao(prServ.findById(id));
		
		ColisDto colis = cServ.findById((Long)model.getAttribute("currentColis"));
		Long idColis = colis.getIdColis();
		etape.setColis(colis);
		
		elServ.save(etape);
		etape = elServ.findByIdColisAndIdPointRelais(idColis, id);
		
		model.addAttribute("currentEtapeLivraison", etape.getIdEtape());

		colis.setEmetteur(uServ.findById(((Long)model.getAttribute("currentEmetteur"))));
		colis.setDestinataire(uServ.findById(((Long) model.getAttribute("currentDestinataire"))));
		colis.setEtapesLivraison(new ArrayList<EtapeLivraisonDto>(Arrays.asList(etape)));

		cServ.save(colis);
		uServ.sendMail(colis.getEmetteur(), colis);
		uServ.sendMail(colis.getDestinataire(), colis);
		


		return "redirect:/colis/new/statut";	
	}
	
	@GetMapping("/new/statut")
	public String choixStatut(Model model) {
		model.addAttribute("statutList", sServ.gatAllStatut());
		model.addAttribute("action", "/colis/new/statut");
		return "etapeStatutForm";
	}
	
	@PostMapping("/new/statut")
	public String choixStatut(@RequestParam("statut") String idStatut, RedirectAttributes atts, Model model) {
		

		Long idEtape = (Long) model.getAttribute("currentEtapeLivraison");
		EtapeLivraisonDto etape = elServ.findById(idEtape);
		List<StatutEtapeDto> statutList = (List<StatutEtapeDto>) model.getAttribute("statutEtapeList");
		Optional<StatutEtapeDto> statutEtape1 = statutList.stream()
														 .filter(s -> Long.parseLong(idStatut) == s.getIdStatut())
														 .findAny();
		
		etape.setStatutEtape(statutEtape1.get());
		elServ.save(etape);
		atts.addFlashAttribute("colis", etape.getColis().getCodeBarre());
		return "redirect:/colis/new/success";	
	}
	
	
	@GetMapping("/new/success")
	public String colisSucces() {
		return "colisSuccess";
	}

/* 	FIN METHODE CREATION COLIS
 * -------------------------------------------------------------------------------------------------------------------------------------------------------------------
   ESPACE COLIS LIES AU TRANSPORTEUR NON LIVRE
 */
	
	@GetMapping("/mes-colis")
	public String colisTransporteur(Model model) {

		return "cmes-colisSearch";
	}
	
	@GetMapping("/mes-colis/recherche")
	public String rechercheColisTransporteur(Model model,
			@RequestParam(name="search", defaultValue = "") String search) {
	
		Long idTransporteur = (Long) model.getAttribute("currentTransporteur");
		List<ColisDto> cDtoList = cServ.findColisAttachToTransporteurWhereCodeBarreContrains(idTransporteur, search); 
		System.out.println("liste dans le deuxieme get : "+cDtoList);
		System.out.println("id transporteur"+ idTransporteur);
		
		model.addAttribute("colisTransporteur", cDtoList);
		
		if(!cDtoList.isEmpty()) {
			model.addAttribute("recherche", "value");
		} else {
			model.addAttribute("recherche", "noValue");
		}
		return "cmes-colisSearch";
	}
	@GetMapping("/mes-colis/{id}/delete")
	public String mesColisDelete(@PathVariable(name = "id") String idColis, Model model) {
		cServ.delete(Long.parseLong(idColis));
		return "redirect:/colis/mes-colis/recherche";
		
	}
	
	@GetMapping("/mes-colis/{id}")
	public String viewColisTransporteur(@PathVariable(name = "id") String idColis, Model model) {
		ColisDto colis = cServ.findById(Long.parseLong(idColis));
		List<EtapeLivraisonDto> etapeList =  colis.getEtapesLivraison();
		List<EtapeLivraisonDto> etapeLivre = etapeList.stream()
													  .filter(e ->  "LIVRE".equals(e.getStatutEtape().getStatutName()))
													  .collect(Collectors.toList());
		System.out.println("etape etapeLivre"+etapeLivre );
		List<EtapeLivraisonDto> etapeTransit =  etapeList.stream()
														 .filter(e ->  "EN TRANSIT".equals(e.getStatutEtape().getStatutName()))
														 .collect(Collectors.toList());
		System.out.println("etape etapeTransit"+etapeTransit );
		List<EtapeLivraisonDto> etapeAttente =  etapeList.stream()
														 .filter(e ->  "EN ATTENTE".equals(e.getStatutEtape().getStatutName()))
														 .collect(Collectors.toList());
		System.out.println("etape etapeAttente"+etapeAttente );
		
		model.addAttribute("colis", colis);
		model.addAttribute("nombreEtape", etapeList.size());
		model.addAttribute("etapeLivre", etapeLivre );
		model.addAttribute("etapeTransit", etapeTransit );
		model.addAttribute("etapeAttente", etapeAttente );
		model.addAttribute("currentColis", Long.parseLong(idColis));
		
		return "cmes-colisViewColis";
	}
	
	@GetMapping("/mes-colis/{id}/delete/{idEtape}")
	public String deleteEtape(@PathVariable(name = "id") String idColis, @PathVariable(name = "idEtape") String idEtape) {
		ColisDto cdto = cServ.findById(Long.parseLong(idColis));
		Long id = Long.parseLong(idEtape);
		List<EtapeLivraisonDto> listEtape = cdto.getEtapesLivraison();
		List<EtapeLivraisonDto> listEtape2 = listEtape.stream()
													  .filter(e -> e.getIdEtape() != id)
													  .collect(Collectors.toList());
		cdto.setEtapesLivraison(listEtape2);
		cServ.save(cdto);
			
		return "redirect:/colis/mes-colis/"+idColis;
	}
	
	
	@GetMapping("/mes-colis/{id}/{idEtape}/{idStatut}")
	public String viewColisTransporteur(@PathVariable(name = "id") String idColis, @PathVariable(name = "idEtape") String idEtape,@PathVariable(name = "idStatut") String idStatut, Model model) {
		EtapeLivraisonDto etape = elServ.findById(Long.parseLong(idEtape));
		StatutEtapeDto statut = sServ.findById(Long.parseLong(idStatut));
		etape.setStatutEtape(statut);
		elServ.save(etape);
		ColisDto colis = cServ.findById(Long.parseLong(idColis));
		if("1".equals(idStatut) && etape.isLivraisonFinale()) {
			colis.setLivraisonDestinataire(true);
			colis.setDate(new Date(System.currentTimeMillis()));
			cServ.save(colis);
		}
		return "redirect:/colis/mes-colis/"+idColis;
	}
	
	@GetMapping("/mes-colis/add-etape/pointRelais")
	public String ajoutEtapeColisTransporteur(Model model) {
		model.addAttribute("mode", "/colis/mes-colis/add-etape/pointRelais/recherche");
		model.addAttribute("nouveau", "/colis/mes-colis/add-etape/pointRelais/new");
		model.addAttribute("action", "/colis//mes-colis/add-etape/pointRelais/validationPointRelais");
		return "pointRelaisSearch";
	}
	
	@GetMapping("/mes-colis/add-etape/pointRelais/recherche")
	public String  ajoutEtapeColisTransporteursearchPr(Model model,
			@RequestParam(name="page", defaultValue = "0") int page,
			@RequestParam(name="size", defaultValue = "80") int size,
			@RequestParam(name="search", defaultValue = "") String search) {
		
		model.addAttribute("mode", "/colis/mes-colis/add-etape/pointRelais/recherche");
		model.addAttribute("nouveau", "/colis/mes-colis/add-etape/pointRelais/new");
		model.addAttribute("action", "/colis//mes-colis/add-etape/pointRelais/validationPointRelais");
		Page<PointRelaisDto> pointRelaisPage = prServ.findByVille(search, PageRequest.of(page, size));
		
		if(!pointRelaisPage.isEmpty()) {
			model.addAttribute("listePointRelais", pointRelaisPage);
			model.addAttribute("pages", new int[pointRelaisPage.getTotalPages()]);
			model.addAttribute("currentPage", page);
			model.addAttribute("keyword", search);
			model.addAttribute("size", size);
			model.addAttribute("recherche", "value");
		} else {
			model.addAttribute("recherche", "noValue");
		}
		return "pointRelaisSearch";
	}

	
	@GetMapping("/mes-colis/add-etape/pointRelais/validationPointRelais")
	public String ajoutEtapeColisTransporteurvalidationPointRelais(Model model, @RequestParam(name = "id") String search) {
		model.addAttribute("currentPointRelais", Long.parseLong(search));
		return "redirect:/colis/mes-colis/add-etape/etape";
	}
	
	
	@GetMapping("/mes-colis/add-etape/pointRelais/new")
	public String ajoutEtapeColisTransporteurcreatePointRelais(Model model) {
		model.addAttribute("pointRelaisBean", new PointRelaisBean());
		model.addAttribute("mode", "new");
		model.addAttribute("action", "/colis/mes-colis/add-etape/pointRelais/new");
		model.addAttribute("retour", "/colis/mes-colis/add-etape/pointRelais");
		return "pointRelaisNew";
	}

	
	@PostMapping("/mes-colis/add-etape/pointRelais/new")
	public String ajoutEtapeColisTransporteurcreatePointRelais(@Valid PointRelaisBean pointRelaisBean, BindingResult br, RedirectAttributes atts, Model model) {
		System.out.println(pointRelaisBean);
		if (br.hasErrors()) {
			model.addAttribute("action", "/colis/mes-colis/add-etape/pointRelais/new");
			model.addAttribute("retour", "/colis/mes-colis/add-etape/pointRelais");
			return "pointRelaisNew";
		}
		prServ.save(pointRelaisBean);
		model.addAttribute("currentPointRelais", prServ.findPointRelais(pointRelaisBean)); 
		return "redirect:/colis/mes-colis/add-etape/etape";
	}

	@GetMapping("/mes-colis/add-etape/etape")
	public String  ajoutEtapeColisTransporteurcreateEtape(Model model) {
		ColisDto colis = cServ.findById((Long)model.getAttribute("currentColis"));
		model.addAttribute("numEtape", colis.getEtapesLivraison().size()+1);
		model.addAttribute("etape", EtapeLivraisonDto.builder().build());
		model.addAttribute("action", "/colis/mes-colis/add-etape/etape");
		model.addAttribute("statutEtapeList", sServ.gatAllStatut());
		
		
		return "etapeForm";
	}

	
	@PostMapping("/mes-colis/add-etape/etape")
	public String ajoutEtapeColisTransporteurcreateEtape(EtapeLivraisonDto etape, BindingResult br, Model model ) {
		System.out.println("etape dans mes-colis A la sortie du four : "+etape);

		if (br.hasErrors()) {
			System.out.println("Errors sa maman");
			model.addAttribute("etape", etape);
			model.addAttribute("action", "/colis/mes-colis/add-etape/etape");
			return "etapeForm";
		}
		
		etape.setTransporteur(tServ.findById((Long)model.getAttribute("currentTransporteur")));
		Long id = (Long) model.getAttribute("currentPointRelais");
		etape.setPointRelaisDao(prServ.findById(id));
		
		ColisDto colis = cServ.findById((Long)model.getAttribute("currentColis"));
		Long idColis = colis.getIdColis();
		etape.setColis(colis);
		
		elServ.save(etape);
		etape = elServ.findByIdColisAndIdPointRelais(idColis, id);
		
		model.addAttribute("currentEtapeLivraison", etape.getIdEtape());
		List<EtapeLivraisonDto> etapescolis = colis.getEtapesLivraison();
		etapescolis.add(etape);
		
		colis.setEtapesLivraison(etapescolis);

		cServ.save(colis);
		return "redirect:/colis/mes-colis/add-etape/statut";	
	}
	
	@GetMapping("/mes-colis/add-etape/statut")
	public String ajoutEtapeColisTransporteurchoixStatut(Model model) {
		model.addAttribute("statutList", sServ.gatAllStatut());
		model.addAttribute("action", "/colis/mes-colis/add-etape/statut");

		return "etapeStatutForm";
	}
	
	@PostMapping("/mes-colis/add-etape/statut")
	public String ajoutEtapeColisTransporteurchoixStatut(@RequestParam("statut") String idStatut, RedirectAttributes atts, Model model) {
		

		Long idEtape = (Long) model.getAttribute("currentEtapeLivraison");
		EtapeLivraisonDto etape = elServ.findById(idEtape);
		List<StatutEtapeDto> statutList = (List<StatutEtapeDto>) model.getAttribute("statutEtapeList");
		Optional<StatutEtapeDto> statutEtape1 = statutList.stream()
														 .filter(s -> Long.parseLong(idStatut) == s.getIdStatut())
														 .findAny();
		
		etape.setStatutEtape(statutEtape1.get());
		elServ.save(etape);
		atts.addFlashAttribute("colis", etape.getColis().getCodeBarre());
		
		String idColis = String.valueOf((Long)model.getAttribute("currentColis"));
		
		return "redirect:/colis/mes-colis/"+ idColis;	
	}
	
	
	
/* 	FIN ESPACE COLIS LIES AU TRANSPORTEUR NON LIVRE
 * -------------------------------------------------------------------------------------------------------------------------------------------------------------------
   ESPACE COLIS LIVRE  */
   
   @GetMapping("/livres")
   public String getColisLivres(Model model) {
	   List<ColisDto> listColisDto = cServ.findColisLivresParTransporteur();
	   model.addAttribute("colisTransporteur", listColisDto);
	   
	   return "colisLivresSearch";
   }
   
   
   @GetMapping("/livres/recherche")
	public String rechercheColisLivres(Model model,
			@RequestParam(name="search", defaultValue = "") String search) {

		List<ColisDto> listColisDto = cServ.findColisLivresParTransporteurAvecCodeBarre(search); 
		
		model.addAttribute("colisTransporteur", listColisDto);
		
		if(!listColisDto.isEmpty()) {
			model.addAttribute("recherche", "value");
		} else {
			model.addAttribute("recherche", "noValue");
		}
		return "colisLivresSearch";
	}
   
   
   @GetMapping("/livres/{id}")
	public String viewColisLivres(@PathVariable(name = "id") String idColis, Model model) {
		ColisDto colisDto = cServ.findById(Long.parseLong(idColis));
		model.addAttribute("colis", colisDto);
		model.addAttribute("codeBarre", colisDto.getCodeBarre());
		model.addAttribute("poids", colisDto.getPoids());
		model.addAttribute("nomEmetteur", colisDto.getEmetteur());
		model.addAttribute("nomDestinataire", colisDto.getDestinataire());

		
		return "colisLivres";
	}
   
	@GetMapping("/livres/{id}/delete")
	public String colisLivresDelete(@PathVariable(name = "id") String idColis, Model model) {
		cServ.delete(Long.parseLong(idColis));
		return "redirect:/colis/livres/recherche";
		
	}
   
 /*   FIN ESPACE COLIS LIVRES
   -------------------------------------------------------------------------------------------------------------------------------------------------------------------
 		ESPACE COLIS EN ATTENTE DE TRANSPORTEUR
 */
   
   
   @GetMapping("/en-attente")
   public String colisAttenteSearch(Model model) {
	   	return "colisAttente";
   }
   
   @GetMapping("/en-attente/recherche")
   public String colisAttenteSearch(Model model,
			@RequestParam(name="search", defaultValue = "") String search) {
	   
		List<ColisDto> listColisDto = cServ.findColisEnAttente(search); 
		System.out.println(listColisDto);
		model.addAttribute("colisTransporteur", listColisDto);
		
		if(!listColisDto.isEmpty()) {
			model.addAttribute("recherche", "value");
		} else {
			model.addAttribute("recherche", "noValue");
		}

	   
	   return "colisAttente";
   }
	
   @GetMapping("/en-attente/{id}")
   public String colisAttenteView(@PathVariable(name = "id") String idColis, Model model) {
	   
	   ColisDto colis = cServ.findById(Long.parseLong(idColis));
	   model.addAttribute("currentColis", colis.getIdColis());
	   model.addAttribute("colis", colis);
	   model.addAttribute("nombreEtape", colis.getEtapesLivraison().size());
	   Optional<EtapeLivraisonDto> etape = colis.getEtapesLivraison().stream()
			   										.reduce((a,b) -> b);
	   if (etape.isPresent()) {
		   model.addAttribute("e", etape.get());
	   }
	   
	   return "colisAttenteViewColis";
   }



}	

