package fr.afpa.livcolis.controller.beans;


import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder


public class TransBean {
	
	private Long idTransporteur;
	@NotBlank @Size(min = 4)
	private String login;
	@NotBlank(message = "Champ obligatoire") @Size(min = 4)
	private String password;
	@NotBlank(message = "Champ obligatoire") @Size(min = 2)
	private String nom;
	@NotBlank(message = "Champ obligatoire") @Email
	private String mail;
	@NotBlank(message = "Champ obligatoire") @Size(min = 10) 
	private String telephone;
	
}