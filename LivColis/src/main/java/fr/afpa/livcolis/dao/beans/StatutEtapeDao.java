package fr.afpa.livcolis.dao.beans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import fr.afpa.livcolis.controller.beans.ColisBean;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
public class StatutEtapeDao {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="statut_id_generator")
	@SequenceGenerator(name="statut_id_generator", sequenceName="statut_seq", allocationSize = 1, initialValue = 1)
	@Column(name="id_statut")
	private long idStatut;
	
	@Column(nullable = false)
	private String statutName;
}
