package fr.afpa.livcolis.dao.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;


import fr.afpa.livcolis.dao.beans.TransDao;

public interface TransporteurPersistance extends JpaRepository<TransDao, Long> {


	Optional<TransDao> findByLogin(String login);



}
