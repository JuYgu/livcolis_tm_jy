package fr.afpa.livcolis.dao.beans;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;

import fr.afpa.livcolis.controller.beans.ColisBean;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
public class ColisDao {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "colis_seq_gen")
	@SequenceGenerator(name = "colis_seq_gen", sequenceName = "colis_seq", initialValue = 1, allocationSize = 1 )
	private Long idColis;
	private Date date;
	private float poids;
	private float prix;
	private String codeBarre;
	private String path;
	private boolean livraisonDestinataire;
	 
	@ManyToOne
	@JoinColumn(name = "idEmetteur", referencedColumnName = "idUser")
	private UserDao emetteur;
	@ManyToOne
	@JoinColumn(name = "idDestinataire", referencedColumnName = "idUser")
	private UserDao destinataire;
	
	@OneToMany(mappedBy = "colis", fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST}, orphanRemoval = true)
	@OrderBy("num ASC")
	private List<EtapeLivraisonDao> etapesLivraison;
	
}
