package fr.afpa.livcolis.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.livcolis.dao.beans.StatutEtapeDao;

public interface StatutEtapePersistance extends JpaRepository<StatutEtapeDao, Long> {

}