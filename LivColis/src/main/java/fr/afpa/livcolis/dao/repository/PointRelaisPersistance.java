package fr.afpa.livcolis.dao.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.livcolis.dao.beans.PointRelaisDao;

public interface PointRelaisPersistance extends JpaRepository<PointRelaisDao, Long>{

	Page<PointRelaisDao> findByVilleContains(String ville, Pageable page);
	PointRelaisDao findByVilleAndVoie(String ville, String voie);
	
}