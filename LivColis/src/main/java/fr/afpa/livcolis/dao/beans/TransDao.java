package fr.afpa.livcolis.dao.beans;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import fr.afpa.livcolis.controller.beans.ColisBean;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
public class TransDao {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transporteur_seq_gen")
	@SequenceGenerator(name = "transporteur_seq_gen", sequenceName = "transporteur_seq" ,initialValue = 1, allocationSize = 1)
	private Long idTransporteur;
	private String login;
	private String password;
	private String nom;
	private String mail;
	private String telephone;
	
	@OneToMany(mappedBy = "transporteur", fetch = FetchType.EAGER)
	private List<EtapeLivraisonDao> etapeLivList;
}
