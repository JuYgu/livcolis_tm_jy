package fr.afpa.livcolis.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.afpa.livcolis.dao.beans.EtapeLivraisonDao;

public interface EtapeLivPersistance extends JpaRepository<EtapeLivraisonDao, Long> {
	
	
	@Query("select e from EtapeLivraisonDao e where e.colis.idColis = ?1 and e.pointRelaisDao.idPointRelais = ?2")
	EtapeLivraisonDao findByColisDao_IdColisAndPointRelaisDao_idPointRelais(Long idColis, Long idPointRelais);

}
