package fr.afpa.livcolis.dao.beans;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import org.springframework.format.annotation.DateTimeFormat;

import fr.afpa.livcolis.controller.beans.ColisBean;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
public class EtapeLivraisonDao {

	@Id 
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "etape_seq_gen")
	@SequenceGenerator(name = "etape_seq_gen", sequenceName = "etape_seq" ,initialValue = 1, allocationSize = 1)
	private Long idEtape;
	private int num;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date date;
	private String description;
	private boolean livraisonTerminee;
	private boolean livraisonFinale;
	
	@ManyToOne
	@JoinColumn(name = "idTransporteur")
	private TransDao transporteur;
	
	@ManyToOne
	@JoinColumn(name = "idColis")
	private ColisDao colis;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idPointRelais")
	private PointRelaisDao pointRelaisDao;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_statut", referencedColumnName = "id_statut")
	private StatutEtapeDao statutEtape;
}
