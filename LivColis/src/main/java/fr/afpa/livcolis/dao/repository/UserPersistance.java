package fr.afpa.livcolis.dao.repository;



import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.livcolis.dao.beans.UserDao;
import fr.afpa.livcolis.services.beans.UserDto;

public interface UserPersistance extends JpaRepository<UserDao, Long> {
	
	Page<UserDao> findByNomContains(String nom,  Pageable page);
	UserDao findByNomAndPrenomAndMail(String nom, String prenom, String mail );
}
