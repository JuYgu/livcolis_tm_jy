package fr.afpa.livcolis.dao.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.afpa.livcolis.dao.beans.ColisDao;

public interface ColisPersistance extends JpaRepository<ColisDao, Long> {
	
	ColisDao findByCodeBarre(String codeBarre);
	
	@Query("FROM ColisDao c inner join EtapeLivraisonDao e ON e.colis.idColis = c.idColis where e.transporteur.idTransporteur = ?1")
	List<ColisDao> findColisAttachToTransporteur(Long idTransporteur);
	
	@Query("select distinct c FROM ColisDao c inner join EtapeLivraisonDao e ON e.colis.idColis = c.idColis where e.transporteur.idTransporteur = :idt and c.codeBarre like concat('%',:cb,'%') and c.livraisonDestinataire is false")
	List<ColisDao> findColisAttachToTransporteurWhereCodeBarreContrains( @Param("idt") Long idTransporteur, @Param("cb") String codeBarre);
	

	@Query("FROM ColisDao c where c.livraisonDestinataire = true")
	List<ColisDao> findColisLivresParTransporteur();
	 
	
	@Query("FROM ColisDao c where c.codeBarre like concat('%',?1,'%') and c.livraisonDestinataire = true")
	List<ColisDao> findColisLivresParTransporteurAvecCodeBarre(String CodeBarre);

	Optional<ColisDao> findDistinctByCodeBarre(String codeBarre);

	@Query("select distinct c FROM ColisDao c inner join EtapeLivraisonDao e ON e.colis.idColis = c.idColis  where c.livraisonDestinataire = false and c.codeBarre like concat('%',:cb,'%') and e.statutEtape.idStatut = 1 and e.colis.idColis not in (select distinct et.colis.idColis FROM EtapeLivraisonDao et where et.statutEtape > 1)")
	List<ColisDao> findColisEnAttente(String cb);

}