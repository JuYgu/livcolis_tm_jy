package fr.afpa.livcolis.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.afpa.livcolis.controller.beans.UserBean;
import fr.afpa.livcolis.dao.beans.UserDao;
import fr.afpa.livcolis.services.beans.UserDto;

@Service
public class UserServiceMappeur {
	@Autowired
	ModelMapper mp;
	
	public UserDao userBeanToDao(UserBean userBean) {

		return  mp.map(userBean, UserDao.class);
		
	}

	public Page<UserDto> pageUDaoToPageUDto(Page<UserDao> pageUserDao, PageRequest pr){

		List<UserDao> uDaoList = pageUserDao.getContent();
		List<UserDto> uDtoList = uDaoList.stream()
							  			 .map(this::mapToUDto)
							  			 .collect(Collectors.toList());

		/*
		 * List<UserDto> uDtoList2 = new ArrayList<UserDto>(); for ( UserDao uDao :
		 * uDaoList) { UserDto uDto2 = mapToUDto(uDao); uDtoList2.add(uDto2); }
		 */
		
		
		
		
		Pageable pageable = pr.of(pr.getPageNumber(), pr.getPageSize());
		int total = uDaoList.size();


		Page<UserDto> uDtoPage = new PageImpl<>(uDtoList, pageable,total );
		return uDtoPage;

	}

	public UserDao uDtoToDao(UserDto uDto) {
		return mp.map(uDto, UserDao.class);
	}
	
	public UserDto mapToUDto(UserDao uDao) {
		return mp.map(uDao, UserDto.class);
	}
	
}


