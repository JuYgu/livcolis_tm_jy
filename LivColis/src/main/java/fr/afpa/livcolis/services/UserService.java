package fr.afpa.livcolis.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import fr.afpa.livcolis.controller.beans.UserBean;
import fr.afpa.livcolis.services.beans.ColisDto;
import fr.afpa.livcolis.services.beans.UserDto;

public interface UserService {
	void save(UserBean userBean);
	Long findUser(UserBean userBean);
	Page<UserDto> findByNom(String nom, PageRequest pr);
	UserDto findById(Long id);
	void update(UserDto uDto);
	void delete(Long idUser);
	void sendMail(UserDto user, ColisDto colis);
}
