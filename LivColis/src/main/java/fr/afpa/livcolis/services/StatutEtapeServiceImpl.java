package fr.afpa.livcolis.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import fr.afpa.livcolis.dao.repository.StatutEtapePersistance;
import fr.afpa.livcolis.services.beans.StatutEtapeDto;

@Service
@Qualifier("statutEtapeService")
public class StatutEtapeServiceImpl implements StatutEtapeService {

	@Autowired
	private StatutEtapePersistance statutEtapePersist;
	@Autowired
	private StatutEtapeServiceMappeur seMap;

	@Override
	public List<StatutEtapeDto> gatAllStatut() {
		return seMap.setDaoListToDtoList(statutEtapePersist.findAll());
	}

	@Override
	public StatutEtapeDto findById(long id) {
		return seMap.mapToSeDto(statutEtapePersist.findById(id).get());
	}
	
}