package fr.afpa.livcolis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import fr.afpa.livcolis.dao.beans.EtapeLivraisonDao;
import fr.afpa.livcolis.dao.repository.EtapeLivPersistance;
import fr.afpa.livcolis.services.beans.EtapeLivraisonDto;

@Service
@Qualifier("etapeLivService")
public class EtapeLivServiceImpl implements EtapeLivService{

	@Autowired
	private EtapeLivPersistance etapePersist;
	@Autowired
	private EtapeLivMappeur eMap;
	
	@Override
	public void save(EtapeLivraisonDto eDto) {
		EtapeLivraisonDao eDao = eMap.eDtoToEDao(eDto);
		etapePersist.save(eDao);
	}

	@Override
	public EtapeLivraisonDto findById(Long id) {
		return eMap.eDaoToEDto(etapePersist.findById(id).get());
	}

	@Override
	public EtapeLivraisonDto findByIdColisAndIdPointRelais(Long idColis, Long idPointRelais) {
		EtapeLivraisonDao eDao = etapePersist.findByColisDao_IdColisAndPointRelaisDao_idPointRelais(idColis, idPointRelais);
		return eMap.eDaoToEDto(eDao);
	}


	
	

}
