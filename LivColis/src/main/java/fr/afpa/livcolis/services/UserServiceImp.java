package fr.afpa.livcolis.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import fr.afpa.livcolis.controller.beans.UserBean;
import fr.afpa.livcolis.dao.beans.UserDao;
import fr.afpa.livcolis.dao.repository.UserPersistance;
import fr.afpa.livcolis.services.beans.ColisDto;
import fr.afpa.livcolis.services.beans.UserDto;
import lombok.Getter;
import lombok.Setter;

@Service
@Qualifier("userService")
public class UserServiceImp implements UserService {

	@Autowired
	private UserPersistance userPersist;
	@Autowired
	private UserServiceMappeur uMap;
	
	@Override
	public void save(UserBean userBean) {
		UserDao uDao = uMap.userBeanToDao(userBean);
		userPersist.save(uDao);
		
	}
	
	@Override
	public Page<UserDto> findByNom(String nom, PageRequest pr) {
		Page<UserDao> userDaoList = userPersist.findByNomContains(nom, pr);
		Page<UserDto> userDtoList = Page.empty();
		if ( !userDaoList.isEmpty()) {
			userDtoList =  uMap.pageUDaoToPageUDto(userDaoList, pr);
			

		}
		return userDtoList;
	}

	@Override
	public void update(UserDto uDto) {
		UserDao uDao = uMap.uDtoToDao(uDto);
		userPersist.save(uDao);
		
	}

	@Override
	public void delete(Long idUser) {
		userPersist.deleteById(idUser);	
	}



	@Override
	public UserDto findById(Long id) {
		return uMap.mapToUDto(userPersist.findById(id).get());

	}

	@Override
	public Long findUser(UserBean userBean) {
		return uMap.mapToUDto(
					userPersist.findByNomAndPrenomAndMail(userBean.getNom(), userBean.getPrenom(), userBean.getMail()))
				.getIdUser();

	}

	@Override
	public void sendMail(UserDto user, ColisDto colis) {
		UserMail.sendMail(user, colis);
		
	}



}
