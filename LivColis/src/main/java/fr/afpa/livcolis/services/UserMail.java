package fr.afpa.livcolis.services;

import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import fr.afpa.livcolis.services.beans.ColisDto;
import fr.afpa.livcolis.services.beans.UserDto;



public interface UserMail {

	public static void sendMail(UserDto user, ColisDto colis){
		 final String username = "javamail.testhotel@gmail.com";
	     final String password = "Azerty123!!!";
	
	     System.out.println("Scrtch *** Scrtch *** lancement des turbines avant envoi du mail ");
	     
	     Properties prop = new Properties();
			prop.put("mail.smtp.host", "smtp.gmail.com");
	     prop.put("mail.smtp.port", "465");
	     prop.put("mail.smtp.auth", "true");
	     prop.put("mail.smtp.socketFactory.port", "465");
	     prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	
	     Session session = Session.getInstance(prop,
	             new javax.mail.Authenticator() {
	                 protected PasswordAuthentication getPasswordAuthentication() {
	                     return new PasswordAuthentication(username, password);
	                 }
	             });
	
	     try {
		     System.out.println("d�sactivation des soupapes de s�curit�s");


	         Message message = new MimeMessage(session);
	         Multipart multipart = new MimeMultipart();
	         MimeBodyPart partieMessage = new MimeBodyPart();
	         
	         String messageColis = "Cher(e) "+user.getNom()+" "+user.getPrenom()
             +"\n\n Un colis a votre nom a été envoyé."
             +"\n\n\n\n Le numéro de colis est le suivant : N°"+colis.getCodeBarre()+"."
             +"\n\n\n\n Vous pouvez suivre en direct l'avancée de votre colis  sur ce lien : "
             + "\n<a href='http://localhost:8491/LivColis/'>LivColis.com</a>";
	         
	         
	         partieMessage.setText(messageColis, "UTF8", "html");
	         multipart.addBodyPart(partieMessage);
              
	         
	         // Configuration exp�diteur/Destinataire
	         message.setFrom(new InternetAddress("from@gmail.com"));
	         message.setRecipients(
	                 Message.RecipientType.TO,
	                 InternetAddress.parse(user.getMail())
	         );
	         
		    
		     message.setSubject("Un colis a votre nom est en cours d'expedition");
		     message.setContent(multipart);
	         Transport.send(message);
	
		     System.out.println("JZIIIIIOUUUU mail envoy� avec succ�s");

	
	     } catch (MessagingException e) {
	         e.printStackTrace();
	     }
	}
}
