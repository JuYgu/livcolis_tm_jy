package fr.afpa.livcolis.services;

import fr.afpa.livcolis.services.beans.EtapeLivraisonDto;

public interface EtapeLivService {
	void save(EtapeLivraisonDto eDto);
	EtapeLivraisonDto findById(Long id);
	EtapeLivraisonDto findByIdColisAndIdPointRelais(Long idColis, Long idPointRelais);

}
