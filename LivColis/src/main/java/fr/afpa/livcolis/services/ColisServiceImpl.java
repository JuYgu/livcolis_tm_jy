package fr.afpa.livcolis.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import fr.afpa.livcolis.controller.beans.ColisBean;
import fr.afpa.livcolis.dao.beans.ColisDao;
import fr.afpa.livcolis.dao.repository.ColisPersistance;
import fr.afpa.livcolis.services.beans.ColisDto;
@Service
@Qualifier("colisService")
public class ColisServiceImpl implements ColisService {

	@Autowired
	private ColisPersistance colisPersist;
	@Autowired
	private ColisServiceMappeur cMap;
	
	
	/**
	 * Permet de générer un code barre au colis que l'on souhaite persister. Une fois le colis enregistré, on le récupère comme CurrentColis
	 */
	@Override
	public ColisDto save( ColisBean colisBean) {
		colisBean = BarCodeGenerator.generateBarCode(colisBean);
		ColisDao cDao = cMap.cBeanToCDao(colisBean);
		colisPersist.save(cDao);
		return cMap.cDaoToCDto(colisPersist.findByCodeBarre(colisBean.getCodeBarre()));
		
	}


	@Override
	public ColisDto findById(Long id) {
		return cMap.cDaoToCDto(colisPersist.findById(id).get()); 
	}


	@Override
	public void save(ColisDto colisDto) {
		colisPersist.save(cMap.cDtoToCDao(colisDto));
	}


	@Override
	public List<ColisDto> findColisAttachToTransporteur(Long idTransporteur) {
		return cMap.ListDaoToListDto(colisPersist.findColisAttachToTransporteur(idTransporteur));

	}
	
	@Override
	public List<ColisDto> findColisAttachToTransporteurWhereCodeBarreContrains(Long idTransporteur, String codeBarre ) {
		List<ColisDao> daoList = colisPersist.findColisAttachToTransporteurWhereCodeBarreContrains(idTransporteur, codeBarre);
		List<ColisDto> dtoList = cMap.ListDaoToListDto(daoList);
		return dtoList;
		
	}


	@Override
	public List<ColisDto> findColisLivresParTransporteur() {
		return cMap.ListDaoToListDto(colisPersist.findColisLivresParTransporteur());
	}


	@Override
	public List<ColisDto> findColisLivresParTransporteurAvecCodeBarre(String CodeBarre) {
		return cMap.ListDaoToListDto(colisPersist.findColisLivresParTransporteurAvecCodeBarre(CodeBarre));
	}


	@Override
	public Optional<ColisDto> findByCodeBarre(String codeBarre) {
		Optional<ColisDao> cDao = colisPersist.findDistinctByCodeBarre(codeBarre);
		Optional<ColisDto> cDtoOptional = Optional.empty();
		if(cDao.isPresent()) {
			ColisDto colisDto = cMap.cDaoToCDto(cDao.get());
			cDtoOptional = Optional.of(colisDto);
		}
		return cDtoOptional;
		
		
	}


	@Override
	public List<ColisDto> findColisEnAttente(String search) {
		List<ColisDao> cDaoList = colisPersist.findColisEnAttente(search);
		
		return cMap.ListDaoToListDto(cDaoList);
	}


	@Override
	public void delete(long idColis) {
		colisPersist.deleteById(idColis);
	}

}
