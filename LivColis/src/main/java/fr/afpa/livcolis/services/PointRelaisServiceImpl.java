package fr.afpa.livcolis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import fr.afpa.livcolis.controller.beans.PointRelaisBean;
import fr.afpa.livcolis.dao.beans.PointRelaisDao;
import fr.afpa.livcolis.dao.repository.PointRelaisPersistance;
import fr.afpa.livcolis.services.beans.PointRelaisDto;
import lombok.Getter;
import lombok.Setter;

@Service
@Qualifier("pointRelaisService")
public class PointRelaisServiceImpl implements PointRelaisService {

	@Autowired
	private PointRelaisPersistance pointRelaisPersist;
	@Autowired
	private PointRelaisServiceMappeur prMap;
	
	
	
	/**
	 * Redéfinition de la méthode save() de l'interface PointRelaisService
	 * => sauvegarde un nouveau PointRelais
	 * @param pointRelaisBean
	 * @return void
	 */
	@Override
	public void save(PointRelaisBean pointRelaisBean) {
		
		PointRelaisDao prDao = prMap.pointRelaisBeanToDao(pointRelaisBean);
		pointRelaisPersist.save(prDao);
	}

	
	/**
	 * Redéfinition de la méthode findByVille() de l'interface PointRelaisService
	 * => retrouve un PointRelais selon la ville recherchée si existant
	 * @param String ville, PageRequest pr
	 * @return PointRelaisDtoList
	 */
	@Override
	public Page<PointRelaisDto> findByVille(String ville, PageRequest pr) {
		Page<PointRelaisDao> pointRelaisDaoList = pointRelaisPersist.findByVilleContains(ville, pr);
		Page<PointRelaisDto> pointRelaisDtoList = Page.empty();
		if(!pointRelaisDaoList.isEmpty()) {
			pointRelaisDtoList = prMap.pagePrDaoToPagePrDto(pointRelaisDaoList, pr);
		}
		return pointRelaisDtoList;
	}
	
	
	/**
	 * Redéfinition de la méthode update() de l'interface PointRelaisService
	 * => met à jour un PointRelais
	 * @param PointRelaisDto
	 * @return void
	 */
	@Override
	public void update(PointRelaisDto prDto) {
		PointRelaisDao prDao = prMap.prDtoToDao(prDto);
		pointRelaisPersist.save(prDao);
	}
	
	
	/**
	 * Redéfinition de la méthode delete() de l'interface PointRelaisService
	 * => supprime un PointRelais
	 * @param idPointRelais
	 * @return void
	 */
	@Override
	public void delete(Long idPointRelais) {
		pointRelaisPersist.deleteById(idPointRelais);
	}

	
	/**
	 * Redéfinition de la méthode findPointRelais() de l'interface PointRelaisService
	 * => retrouve un Point Relais
	 * @param PointRelaisBean
	 */
	@Override
	public Long findPointRelais(PointRelaisBean pointRelaisBean) {
		return prMap.mapToPrDto(pointRelaisPersist.findByVilleAndVoie(pointRelaisBean.getVille(), pointRelaisBean.getVoie())).getIdPointRelais();

	
	}

	

	
	/**
	 * Redéfinition de la méthode findById() de l'interface PointRelaisService
	 * => retrouve un PointRelais selon l'ID qui lui a été attribué lors de sa création
	 * @param Long id
	 * @return void
	 */
	@Override
	public PointRelaisDto findById(Long id) {
		return prMap.mapToPrDto(pointRelaisPersist.findById(id).get());
	}
	
}