package fr.afpa.livcolis.services.beans;

import java.util.Date;
import java.util.List;

import fr.afpa.livcolis.controller.beans.ColisBean;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class PointRelaisDto {

	private Long idPointRelais;
	private String telephone;
	private String voie;
	private String ville;
	private String codePostal;
	private String ouverture;
	private String fermeture;
	

}
