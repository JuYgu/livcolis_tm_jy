package fr.afpa.livcolis.services;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.livcolis.dao.beans.EtapeLivraisonDao;
import fr.afpa.livcolis.services.beans.EtapeLivraisonDto;



@Service
public class EtapeLivMappeur {
	@Autowired
	ModelMapper mp;
	
	public EtapeLivraisonDao eDtoToEDao(EtapeLivraisonDto eDto) {
		
		return mp.map(eDto, EtapeLivraisonDao.class);
	}
	public EtapeLivraisonDto eDaoToEDto(EtapeLivraisonDao eDao) {
		
		return mp.map(eDao, EtapeLivraisonDto.class);
	}

}
