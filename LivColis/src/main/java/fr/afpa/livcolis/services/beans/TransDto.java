package fr.afpa.livcolis.services.beans;

import java.util.Date;
import java.util.List;

import fr.afpa.livcolis.controller.beans.ColisBean;
import fr.afpa.livcolis.dao.beans.EtapeLivraisonDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class TransDto {
	
	private Long idTransporteur;
	private String login;
	private String password;
	private String nom;
	private String mail;
	private String telephone;
	
	private List<EtapeLivraisonDto> etapeLivList;
	
}