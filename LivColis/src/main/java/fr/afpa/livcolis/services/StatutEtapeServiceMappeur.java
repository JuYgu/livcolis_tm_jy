package fr.afpa.livcolis.services;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.livcolis.dao.beans.StatutEtapeDao;
import fr.afpa.livcolis.services.beans.StatutEtapeDto;

@Service
public class StatutEtapeServiceMappeur {
	@Autowired
	ModelMapper mp;
	
	
	public List<StatutEtapeDto> setDaoListToDtoList(List<StatutEtapeDao> list){
		
		/*
		 * List<StatutEtapeDto> listDto2 = list.stream() .map(this::mapToSeDto)
		 * .collect(Collectors.toList()); } return listDto2; }
		 */
		
		List<StatutEtapeDto> listDto = new ArrayList();
		for (StatutEtapeDao seDao : list) {
			listDto.add(mapToSeDto(seDao));
		}
		return listDto;
	}
	
	
	public StatutEtapeDto mapToSeDto(StatutEtapeDao seDao) {
		return mp.map(seDao, StatutEtapeDto.class);
	}
	
}