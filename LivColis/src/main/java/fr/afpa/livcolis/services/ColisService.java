package fr.afpa.livcolis.services;

import java.util.List;
import java.util.Optional;

import fr.afpa.livcolis.controller.beans.ColisBean;
import fr.afpa.livcolis.dao.beans.ColisDao;
import fr.afpa.livcolis.services.beans.ColisDto;
import fr.afpa.livcolis.services.beans.EtapeLivraisonDto;

public interface ColisService {

	ColisDto save( ColisBean colisBean);
	void save( ColisDto colisDto);
	ColisDto findById(Long id);
	List<ColisDto> findColisAttachToTransporteur(Long idTransporteur);
	List<ColisDto> findColisAttachToTransporteurWhereCodeBarreContrains(Long idTransporteur, String codeBarre);

	List<ColisDto> findColisLivresParTransporteur();
	List<ColisDto> findColisLivresParTransporteurAvecCodeBarre(String CodeBarre);
	Optional<ColisDto> findByCodeBarre(String codeBarre);
	List<ColisDto> findColisEnAttente(String search);
	void delete(long parseLong);
}
