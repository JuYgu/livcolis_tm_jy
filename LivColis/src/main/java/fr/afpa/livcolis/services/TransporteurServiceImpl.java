package fr.afpa.livcolis.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.util.Optionals;
import org.springframework.stereotype.Service;

import fr.afpa.livcolis.controller.beans.TransBean;
import fr.afpa.livcolis.controller.beans.TransConnBean;
import fr.afpa.livcolis.dao.beans.TransDao;
import fr.afpa.livcolis.dao.repository.TransporteurPersistance;
import fr.afpa.livcolis.services.beans.TransDto;


@Service
@Qualifier("transporteurService")
public class TransporteurServiceImpl implements TransporteurService {

	@Autowired
	private TransporteurPersistance transPersist;
	@Autowired
	private TransporteurServiceMappeur tMap;
 
	
	
	
	@Override
	public boolean save(TransBean transporteurBean) {
		Optional<TransDao> OptionTDao = transPersist.findByLogin(transporteurBean.getLogin());
		
		if(!OptionTDao.isPresent()) {
			TransDao tDao = tMap.transBeanToDao(transporteurBean);
			transPersist.save(tDao);
			return true;
		}
		return false;
	}


	@Override
	public Optional<TransDto> findByLogin(TransConnBean transporteurConnexion) {
		
		Optional<TransDao> OptionTDao = transPersist.findByLogin(transporteurConnexion.getLogin());
		Optional<TransDto> OptionTDto = Optional.empty();
		if ( OptionTDao.isPresent()) {
			if(transporteurConnexion.getPassword().equals(OptionTDao.get().getPassword())) {
				OptionTDto = Optional.of(tMap.tDaoTotDto(OptionTDao.get()));

			}

		}
		
		return OptionTDto;
		
	}


	/**
	 * Update un Transporteur dans la BDD et le récupère comme currentTransporteur
	 */
	@Override
	public void update(TransBean transporteurDto) {
		TransDao tDao = tMap.transBeanToDao(transporteurDto);
		transPersist.save(tDao);
	}


	/**
	 * Permet de récupérer un transporteur de la BDD avec un login et de l'ajouter en Current Transporteur
	 */
	@Override
	public TransDto findById(Long id) {
		Optional<TransDao> tDao = transPersist.findById(id);
		TransDto tDto = null;
		if(tDao.isPresent()) {
			tDto = tMap.tDaoTotDto(tDao.get());
		}

		return tDto;
		
	}


	@Override
	public void delete(Long id) {
		transPersist.deleteById(id);
		
	}

}
