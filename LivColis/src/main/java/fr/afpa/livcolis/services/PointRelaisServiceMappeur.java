package fr.afpa.livcolis.services;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.afpa.livcolis.controller.beans.PointRelaisBean;
import fr.afpa.livcolis.dao.beans.PointRelaisDao;
import fr.afpa.livcolis.services.beans.PointRelaisDto;

@Service
public class PointRelaisServiceMappeur {

	@Autowired
	ModelMapper mp;
	
	/**
	 * Transforme un PointRelais Bean en un PointRelais DAO
	 * @param pointRelaisBean
	 * @return prDao
	 */
	public PointRelaisDao pointRelaisBeanToDao(PointRelaisBean pointRelaisBean) {
		
		PointRelaisDao prDao = mp.map(pointRelaisBean, PointRelaisDao.class);
		return prDao;
	}
	
	/**
	 * Transforme un PointRelais DAO en un PointRelais DTO
	 * @param pagePointRelaisDao
	 * @return prDtoPage
	 */
	public Page<PointRelaisDto> pagePrDaoToPagePrDto(Page<PointRelaisDao> pagePointRelaisDao, PageRequest pr){
		
		List<PointRelaisDao> prDaoList = pagePointRelaisDao.getContent();
		List<PointRelaisDto> prDtoList = prDaoList.parallelStream().map(this::mapToPrDto).collect(Collectors.toList());
		
		Pageable pageable = pr.of(pr.getPageNumber(), pr.getPageSize());
		int total = prDaoList.size();
		
		Page<PointRelaisDto> prDtoPage = new PageImpl<>(prDtoList, pageable, total);
		
		return prDtoPage;
	
	}
	
	/**
	 * Méthode pour mapper un PointRelais DAO vers PointRelais DTO
	 * @param prDao
	 * @return
	 */
	public PointRelaisDto mapToPrDto(PointRelaisDao prDao) {
		return mp.map(prDao, PointRelaisDto.class);
	}
	
	/**
	 * Méthode pour mapper un PointRelais DTO vers PointRelais DAO
	 * @param prDto
	 * @return
	 */
	public PointRelaisDao prDtoToDao(PointRelaisDto prDto) {
		return mp.map(prDto, PointRelaisDao.class);
	}
	
}