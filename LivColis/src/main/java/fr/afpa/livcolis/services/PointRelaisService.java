package fr.afpa.livcolis.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import fr.afpa.livcolis.controller.beans.PointRelaisBean;
import fr.afpa.livcolis.services.beans.PointRelaisDto;


/**
 * Interface pour la gestion des PointRelais
 */
public interface PointRelaisService {

	void save(PointRelaisBean pointRelaisBean);
	Long findPointRelais(PointRelaisBean pointRelaisBean);
	Page<PointRelaisDto> findByVille(String ville, PageRequest pr);
	PointRelaisDto findById(Long id);
	void update(PointRelaisDto prDto);
	void delete(Long idPointRelais);
	
}