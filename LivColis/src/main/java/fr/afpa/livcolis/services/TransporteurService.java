package fr.afpa.livcolis.services;

import java.util.Optional;

import fr.afpa.livcolis.controller.beans.TransBean;
import fr.afpa.livcolis.controller.beans.TransConnBean;
import fr.afpa.livcolis.services.beans.TransDto;

public interface TransporteurService {

	boolean save(TransBean transporteurBean);
	Optional<TransDto> findByLogin(TransConnBean transporteurConnexion);
	TransDto findById(Long id);
	void update(TransBean transporteurBean);
	void delete(Long id);

	
	
}
