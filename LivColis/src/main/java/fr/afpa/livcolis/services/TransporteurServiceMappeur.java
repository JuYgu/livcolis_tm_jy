package fr.afpa.livcolis.services;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.livcolis.controller.beans.TransBean;
import fr.afpa.livcolis.dao.beans.TransDao;
import fr.afpa.livcolis.services.beans.TransDto;

@Service
public class TransporteurServiceMappeur {
	
	@Autowired
	ModelMapper mp;
	
	
	/***
	 * Transforme un transporteur BEAN en transporteur DAO
	 * @param tDto
	 * @return
	 */
	public TransDao transBeanToDao(TransBean transporteurBean) {
		
		TransDao tDao = mp.map(transporteurBean, TransDao.class);
		return tDao;
	}
	
	/***
	 * Transforme un transporteur DAO en transporteur DTO
	 * @param tDto
	 * @return
	 */
	public TransDto tDaoTotDto(TransDao tDao) {
		TransDto tDto = mp.map(tDao, TransDto.class);
		return tDto;
	}
	



}
