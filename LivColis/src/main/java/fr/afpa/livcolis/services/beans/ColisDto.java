package fr.afpa.livcolis.services.beans;

import java.util.Date;
import java.util.List;

import fr.afpa.livcolis.controller.beans.ColisBean;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ColisDto {
	
	private Long idColis;
	private Date date;
	private float poids;
	private float prix;
	private String codeBarre;
	private String path;
	private boolean livraisonDestinataire;
	

	private UserDto emetteur;
	private UserDto destinataire;
	
	@ToString.Exclude
	private List<EtapeLivraisonDto> etapesLivraison;
	
	
	
	
}

