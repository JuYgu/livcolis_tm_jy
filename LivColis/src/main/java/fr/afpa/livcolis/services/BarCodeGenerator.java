package fr.afpa.livcolis.services;

import java.io.File;
import java.util.Random;

import org.springframework.stereotype.Service;

import fr.afpa.livcolis.controller.beans.ColisBean;
import fr.afpa.livcolis.services.beans.ColisDto;
import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.BarcodeFactory;
import net.sourceforge.barbecue.BarcodeImageHandler;
import net.sourceforge.barbecue.output.OutputException;
@Service
public interface BarCodeGenerator {

	public static ColisBean generateBarCode(ColisBean cBean) {
		try {
			String code = generateCode();
			System.out.println(code);
			Barcode barcode = BarcodeFactory.createEAN13(code);
			cBean.setCodeBarre(code);
			File codeBarreImg = new File("/images/"+code+".jpg"); 
			cBean.setPath("/images/"+code+".jpg");
			BarcodeImageHandler.saveJPEG(barcode, codeBarreImg);
			
		} catch (BarcodeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
			 catch (OutputException e) { // TODO Auto-generated catch block
			  e.printStackTrace(); }
			 
		
		return cBean;
	}

	
	
	
	static String generateCode() {
		
		StringBuilder code = new StringBuilder();
		for ( int i = 0; i < 12; i++) {
			code.append(randInt(0,9)+"");
		}
		
		return code.toString();
	}
	
	
	static int randInt(int min, int max) {
		Random rand  = new Random();
	    int randomNum = rand.nextInt((max - min) + 1) + min;
	    return randomNum;
	}
}
