package fr.afpa.livcolis.services;

import java.util.List;

import fr.afpa.livcolis.services.beans.StatutEtapeDto;

public interface StatutEtapeService {

	List<StatutEtapeDto> gatAllStatut();

	StatutEtapeDto findById(long parseLong);
}
