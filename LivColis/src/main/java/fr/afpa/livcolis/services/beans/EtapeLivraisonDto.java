package fr.afpa.livcolis.services.beans;


import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.springframework.format.annotation.DateTimeFormat;

import fr.afpa.livcolis.controller.beans.ColisBean;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class EtapeLivraisonDto {

	private Long idEtape;
	@NotNull @Positive
	private int num;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date date;
	private boolean livraisonTerminee;
	private boolean livraisonFinale;
	
	@ToString.Exclude
	private TransDto transporteur;
	@ToString.Exclude
	private ColisDto colis;
	@ToString.Exclude
	private PointRelaisDto pointRelaisDao;
	@ToString.Exclude
	private StatutEtapeDto statutEtape;
}