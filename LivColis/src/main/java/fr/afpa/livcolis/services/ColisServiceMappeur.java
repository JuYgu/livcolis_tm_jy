package fr.afpa.livcolis.services;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.livcolis.controller.beans.ColisBean;
import fr.afpa.livcolis.dao.beans.ColisDao;
import fr.afpa.livcolis.services.beans.ColisDto;

@Service
public class ColisServiceMappeur {

	@Autowired
	ModelMapper mp;
	
	public ColisDao cBeanToCDao(ColisBean colisBean) {
		
		return mp.map(colisBean, ColisDao.class);
	}

	public ColisDao cDtoToCDao(ColisDto colisDto) {
		
		return mp.map(colisDto, ColisDao.class);
	}
	
	public ColisDto cDaoToCDto(ColisDao colisDao) {
		
		return mp.map(colisDao, ColisDto.class);
	}

	public List<ColisDto> ListDaoToListDto(List<ColisDao> cDaoList) {
		
		return cDaoList.stream()
					   .map(this::cDaoToCDto)
					   .collect(Collectors.toList());
	}

}
