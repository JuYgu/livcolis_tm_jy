package fr.afpa.livcolis.services.beans;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class UserDto {

	private Long idUser;
	private String nom;
	private String prenom;
	private String mail;
	private String telephone;
	private String voie;
	private String codePostal;
	private String ville;
	
	@ToString.Exclude
	private List<ColisDto> colisEmetList;
	@ToString.Exclude
	private List<ColisDto> colisDestList;
}
